from django.db import models

# Create your models here.
class Quote(models.Model):
	quote = models.TextField('quote', null = False, blank = False)
	author = models.CharField('author', max_length=300, null = True, blank = True)

	def __unicode__(self):
		return 'quote ' + str(self.id)

class Test(models.Model):
	quote = models.TextField('quote', null = False, blank = False)
	author = models.CharField('author', max_length=300, null = True, blank = True)

	def __unicode__(self):
		return 'quote ' + str(self.id)
	