from django.contrib import admin
from models import Quote
# Register your models here.

class QuoteAdmin(admin.ModelAdmin):
	fields = ['quote', 'author']
	list_display = ('id', 'quote', 'author')
	ordering = ('id', )
	search_fields = ['quote', 'author']
	list_per_page = 30
	
admin.site.register(Quote, QuoteAdmin)