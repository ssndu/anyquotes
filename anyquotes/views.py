from django.shortcuts import render, redirect
from django.http import HttpResponse
from models import Quote
from random import randint


# Create your views here.
def index(request):
	len = randint(0, Quote.objects.count())
	return redirect('/' + str(len))

def get_quote(request, id):
	quote = Quote.objects.filter(id = id).first()

	if quote is None:
		return HttpResponse('ZHOPA')
	else:
		context = {
				   'quote' : quote.quote,
				   'author': quote.author,
				   'id'	   : quote.id
				   }

		return render(request, 'anyquotes/quote.html', context)