from django.conf.urls import patterns, url
import views
urlpatterns = patterns('',
	url(r'^$', views.index , name = 'index'),
	url(r'^(?P<id>[0-9]+)/$', views.get_quote),
)