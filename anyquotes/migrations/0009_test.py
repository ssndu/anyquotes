# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('anyquotes', '0008_auto_20141011_1858'),
    ]

    operations = [
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quote', models.TextField(verbose_name=b'quote')),
                ('author', models.CharField(max_length=300, null=True, verbose_name=b'author', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
