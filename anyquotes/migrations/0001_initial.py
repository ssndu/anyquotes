# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Quotes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quote', models.TextField(verbose_name=b'quote')),
                ('author', models.CharField(max_length=300, verbose_name=b'author')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
