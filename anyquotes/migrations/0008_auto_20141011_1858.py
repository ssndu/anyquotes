# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('anyquotes', '0007_auto_20141011_1856'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quote',
            name='author',
            field=models.CharField(max_length=300, null=True, verbose_name=b'author', blank=True),
        ),
        migrations.AlterField(
            model_name='quote',
            name='quote',
            field=models.TextField(verbose_name=b'quote'),
        ),
    ]
